# Release Notes

## Development

[Commits](https://bitbucket.org/mensones/desafio/compare/v0.0.1%0Dmaster)

## v0.0.1 - Mar 11th, 2020

New(s):

- 10 days used http/https NodeJs

Change(s):

- No Change(s)

Fix(es):

- No Fix(es)

Compatibility notes:

- No incompatibilities are to be expected

[Commits](link compare latest version with new version)
